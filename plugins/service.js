export default ({ $axios }, inject) => {
  inject('getProductsLi st', async params => await $axios.get('/data.json', {
    params
  }))
}
